# random-user-api #

* promise-based access to random user information
* uses a chaining API to simplify behavior

# Install setup #

* npm install random-user-api --save

# Usage #

## from node ##
```
const {RandomUser} = require('random-user-api')

new RandomUser().format('json').count(1).nationality('de').retrieve()
.then( (data)=> console.log(data))

```

## output ##
```
{
  "results": [
    {
      "gender": "female",
      "name": {
        "title": "miss",
        "first": "sandra",
        "last": "burke"
      },
      "location": {
        "street": "7766 crockett st",
        "city": "boise",
        "state": "virginia",
        "postcode": 56434,
        "coordinates": {
          "latitude": "40.9255",
          "longitude": "75.3897"
        },
        "timezone": {
          "offset": "+2:00",
          "description": "Kaliningrad, South Africa"
        }
      },
      "email": "sandra.burke@example.com",
      "login": {
        "uuid": "d148c21e-884e-4254-92fc-2ade23bd2970",
        "username": "smallcat704",
        "password": "world",
        "salt": "Gp4jtrKj",
        "md5": "10f9d4cf7e951f1836cab65d203bbecb",
        "sha1": "b54d412895e42c6a724f27c89704052f418d7351",
        "sha256": "1ff34c9b85361fd1591e38a634d97941e07e5c2a8faa670af068c0cb08e35a51"
      },
      "dob": {
        "date": "1990-07-02T21:46:18Z",
        "age": 28
      },
      "registered": {
        "date": "2017-01-04T09:53:02Z",
        "age": 1
      },
      "phone": "(278)-164-0581",
      "cell": "(095)-186-3302",
      "id": {
        "name": "SSN",
        "value": "018-62-3079"
      },
      "picture": {
        "large": "https://randomuser.me/api/portraits/women/17.jpg",
        "medium": "https://randomuser.me/api/portraits/med/women/17.jpg",
        "thumbnail": "https://randomuser.me/api/portraits/thumb/women/17.jpg"
      },
      "nat": "US"
    },
    {
      "gender": "female",
      "name": {
        "title": "mrs",
        "first": "birgitt",
        "last": "wiebe"
      },
      "location": {
        "street": "erlenweg 3",
        "city": "steinfurt",
        "state": "brandenburg",
        "postcode": 65366,
        "coordinates": {
          "latitude": "11.8590",
          "longitude": "108.1134"
        },
        "timezone": {
          "offset": "-3:30",
          "description": "Newfoundland"
        }
      },
      "email": "birgitt.wiebe@example.com",
      "login": {
        "uuid": "11f82db6-5140-41ca-850c-f0a2f55a4d39",
        "username": "crazysnake486",
        "password": "nightwin",
        "salt": "woIYNUGi",
        "md5": "6aa8e38aeedb6073b4c93f963b06ca24",
        "sha1": "451af69a32571dff9b9b462b01af239b2af4d217",
        "sha256": "83bdb44d116181c263d8018931a6b023c78f831d48c536c0e3daf41743dbf346"
      },
      "dob": {
        "date": "1986-11-30T10:38:23Z",
        "age": 31
      },
      "registered": {
        "date": "2007-01-04T04:01:57Z",
        "age": 11
      },
      "phone": "0627-4702560",
      "cell": "0174-0196782",
      "id": {
        "name": "",
        "value": null
      },
      "picture": {
        "large": "https://randomuser.me/api/portraits/women/43.jpg",
        "medium": "https://randomuser.me/api/portraits/med/women/43.jpg",
        "thumbnail": "https://randomuser.me/api/portraits/thumb/women/43.jpg"
      },
      "nat": "DE"
    },
    {
      "gender": "female",
      "name": {
        "title": "mrs",
        "first": "rose",
        "last": "kloth"
      },
      "location": {
        "street": "lessingstraße 1",
        "city": "zella-mehlis",
        "state": "sachsen",
        "postcode": 27859,
        "coordinates": {
          "latitude": "-24.1615",
          "longitude": "-93.1599"
        },
        "timezone": {
          "offset": "-12:00",
          "description": "Eniwetok, Kwajalein"
        }
      },
      "email": "rose.kloth@example.com",
      "login": {
        "uuid": "8dac82a4-d6c4-47a4-87b0-38a1701facd4",
        "username": "bigleopard839",
        "password": "jellybea",
        "salt": "WEnB8kPr",
        "md5": "d6e546106f1bfbf15e4f07673b682802",
        "sha1": "49629663f30b183718e491f94f1ed8778f5127b9",
        "sha256": "0ddca87a89cc2aab039ba1e6119ba6066a01ab6448b97fb7a969a33606c0f9fc"
      },
      "dob": {
        "date": "1946-04-04T13:41:44Z",
        "age": 72
      },
      "registered": {
        "date": "2010-10-25T11:30:42Z",
        "age": 8
      },
      "phone": "0899-0267290",
      "cell": "0172-2586486",
      "id": {
        "name": "",
        "value": null
      },
      "picture": {
        "large": "https://randomuser.me/api/portraits/women/68.jpg",
        "medium": "https://randomuser.me/api/portraits/med/women/68.jpg",
        "thumbnail": "https://randomuser.me/api/portraits/thumb/women/68.jpg"
      },
      "nat": "DE"
    }
  ],
  "info": {
    "seed": "6b5e301fc67d294f",
    "results": 3,
    "page": 1,
    "version": "1.2"
  }
}
```

# Usage: limit fields in output #
```
const randomUser = new RandomUser().format('json')

// only output name, email and nationality for 3
randomUser
.excludeAllFieldsBut('name').and()
.excludeAllFieldsBut('email')
.and().excludeAllFieldsBut('nat')
.nationality('us').and().nationality('fr')
.page(3).nationality('us').count(3).retrieve()
.then((res)=>{
  if(randomUser._format==='json') {
    console.log("RES:"+JSON.stringify(res));
    console.log(`number found: ${res.length}`)
    } else {
      console.log('something else')
    }
})
.catch((err)=>console.log(`problem, err=${err}`))
```

## output ##
```
[
  {
    "name": {
      "title": "mr",
      "first": "thomas",
      "last": "muller"
    },
    "email": "thomas.muller@example.com",
    "nat": "FR"
  },
  {
    "name": {
      "title": "mr",
      "first": "robin",
      "last": "lemaire"
    },
    "email": "robin.lemaire@example.com",
    "nat": "FR"
  },
  {
    "name": {
      "title": "mr",
      "first": "ruben",
      "last": "philippe"
    },
    "email": "ruben.philippe@example.com",
    "nat": "FR"
  }
]
number found: 3
```

# available information #

* ![photographs](https://randomuser.me/api/portraits/med/women/68.jpg "Photographs")


# resources / links #

* [nodejs api](https://nodejs.org/api/)
* [axios](https://www.npmjs.com/package/axios)
* [randomuser.me](https://randomuser.me/)

