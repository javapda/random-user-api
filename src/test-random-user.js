const axios = require('axios')
const RandomUser = require('./random-user')
console.log('testing')
//console.log('RandomUser: '+RandomUser)
//console.log(typeof RandomUser)
let randomUser = new RandomUser().format('json')

// randomUser.excludeAllFieldsBut('email').and().excludeAllFieldsBut('name')
// .and().excludeAllFieldsBut('nat')
// .nationality('us').and().nationality('fr')
// .page(3).nationality('us').count(3).retrieve()
// .then((res)=>{
//   if(randomUser._format==='json') {
//     console.log("RES:"+JSON.stringify(res));
//     console.log(`number found: ${res.length}`)
//     } else {
//       console.log('something else')
//     }
// })
// .catch((err)=>console.log(`problem, err=${err}`))

//setTimeout(()=>console.log(randomUser.toString()), 3000)

randomUser
.excludeAllFieldsBut('name').and()
.excludeAllFieldsBut('email')
.and().excludeAllFieldsBut('nat')
.nationality('us').and().nationality('fr')
.page(3).nationality('us').count(3).retrieve()
.then((res)=>{
  if(randomUser._format==='json') {
    console.log(JSON.stringify(res));
    console.log(`number found: ${res.length}`)
    } else {
      console.log('something else')
    }
})
.catch((err)=>console.log(`problem, err=${err}`))