const fs = require('fs')
const _ = require('lodash')
const axios = require('axios')
const path = require('path')
module.exports = class RandomUser {
    constructor() {
        console.log('random user me class')
        this.requestCount=1
        this.rumUrl=`https://randomuser.me/api/`
        this.formats=['json','pretty','csv','yaml','xml']
        this.fields=['gender','name','location',
            'email','login','registered',
            'dob','phone','cell',
            'id','picture','nat']
        this.nationalities=[]
        this.inc=[]
        this.exc=[]
        this._page=1
    }
    and() {
        return this;
    }
    excludeAllFieldsBut(fld) {
        if(!this.fields.includes(fld)) throw `invalid field ${fld}`
        this.exc.includes(fld) || this.exc.push(fld)
        return this
    }
    includeField(fld) {
        if(!this.fields.includes(fld)) throw `invalid field ${fld}`
        this.inc.includes(fld) || this.inc.push(fld)
        return this
    }
    nationality(nationality) {
        this.nationalities.includes(nationality) || this.nationalities.push(nationality)
        return this;
    }
    page(page) {
        this._page = page;
        return this;
    }

    format(format) {
        console.log(this.formats)
        if (!this.formats.includes(format)) {
            throw `invalid format ${format}`
        }
        this._format = format
        
        return this
    }

    count(num) {
        this.requestCount=num
        return this
    }
    
    retrieve() {
        const url=`${this.rumUrl}`
        const props={params:{
            results:this.requestCount,
            page:(this._page?this._page:"json"),
            format:(this._format?this._format:"json")}
        }
        if(this.exc.length>0) props.params.inc=this.exc.join(',')
        if(this.inc.length>0) props.params.inc=this.inc.join(',')
        if(this.nationalities.length>0) props.params.nat=this.nationalities.join(',')
        return new Promise((resolve,reject)=>{
            axios.get(url,props)
            .then((randomUserData)=>{
                if (props.params.format === 'json') {
                    //console.log(`randomUserData=${JSON.stringify(randomUserData.data)}`);
                    resolve(randomUserData.data.results);
                } else {
                    resolve(randomUserData.data);
                }
            })
            .catch((err)=>reject(err))
        })
    }

    toString() {
        return `rumUrl:${this.rumUrl}, format:${this._format}, nationalities:${this.nationalities.join(",")}, page=${this._page}`
    }

    help() {
        return new Promise((resolve,reject)=>{
            fs.readFile(path.resolve(__dirname, './random-user.help.txt'),{encoding:'UTF-8'},(err,data) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(data)
                }
            })
        }
        );
    }

}
